
package rental;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AlbumController {

    @RequestMapping("/album/{albumId}")
    public @ResponseBody Album album(
            @PathVariable(value = "albumId") long albumId) {
        return new Album(albumId, "Dark Side of the Moon");
    }
    
}
