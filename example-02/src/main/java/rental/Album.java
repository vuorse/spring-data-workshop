
package rental;

import java.util.List;


public class Album {
    
    private long id;
    private String name;
    private String artist;
    private String musicStyle;
    private List<String> songs;
    
    public Album(long albumId, String albumName) {
        this.id = albumId;
        this.name = albumName;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getMusicStyle() {
        return musicStyle;
    }

    public List<String> getSongs() {
        return songs;
    }

}
