
package files;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ContainerController {
    
    List<Container> containers = Arrays.asList(new Container(1L, "backups"), new Container(23L, "files"));
    
    @RequestMapping("/containers")
    public @ResponseBody String containers() {
        List<String> containerNames = Lists.transform(containers, new Function<Container, String>() {

            @Override
            public String apply(Container f) {
                return f.getName();
            }
            
        });
        String result = String.format("%s\n", Joiner.on("\n").join(containerNames));
        return result;
    }

}
