
package files;


public class File {
    
    private long id;
    private long containerId;
    private String name;
    private byte[] content;

    public long getId() {
        return id;
    }

    public long getContainerId() {
        return containerId;
    }

    public String getName() {
        return name;
    }

    public byte[] getContent() {
        return content;
    }
    

}
