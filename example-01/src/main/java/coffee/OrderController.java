
package coffee;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {
    
    @RequestMapping("/order/{orderId}")
    public @ResponseBody Order order(
            @PathVariable(value = "orderId") long orderId) {
        return new Order(orderId, "latte", "coconut", 1000L);
    }

}
