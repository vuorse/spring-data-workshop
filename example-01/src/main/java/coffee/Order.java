
package coffee;


public class Order {

    private long id;
    private String drink;
    private String additions;
    private long cost;
    
    public Order(long orderId, String drink, String additions, long cost) {
        id = orderId;
        this.drink = drink;
        this.additions = additions;
        this.cost = cost;
    }

    public long getId() {
        return id;
    }

    public String getDrink() {
        return drink;
    }

    public String getAdditions() {
        return additions;
    }

    public long getCost() {
        return cost;
    }
    
}
