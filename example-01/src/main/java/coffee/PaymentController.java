
package coffee;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PaymentController {

    @RequestMapping("/payment/order/{orderId}")
    public @ResponseBody Payment payment(
        @PathVariable(value = "orderId") long orderId) {
        return new Payment(orderId);
    }
    
}
