
package coffee;


public class Payment {
    
    private long orderId;
    private String cardNo;
    private String expires;
    private String name;
    private long amount;

    public Payment(long orderId) {
        this.orderId = orderId;
    }
    
    public long getOrderId() {
        return orderId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public String getExpires() {
        return expires;
    }

    public String getName() {
        return name;
    }

    public long getAmount() {
        return amount;
    }

}
